# ---------------------------------------------------------------
# Build $(PK)
# ---------------------------------------------------------------

root-verify:
	@if [ $(UID) -ne 0 ]; then \
		$(MSG11) "You must be root." $(EMSG); \
		exit 1; \
	fi

opkg-verify:
	@if [ "$(OPKG_DIR)" = "" ] || [ ! -f $(OPKG_DIR)/opkg-build ]; then \
		$(MSG11) "Can't find opkg-build.  Try setting OPKG= to the directory it lives in." $(EMSG); \
		exit 1; \
	fi

build-verify:

# Retrieve package
$(PK_T)-get: .$(PK_T)-get

.$(PK_T)-get: 
	@touch .$(subst .,,$@)

$(PK_T)-get-patch: .$(PK_T)-get-patch

.$(PK_T)-get-patch: .$(PK_T)-get
	@touch .$(subst .,,$@)

# Unpack packages
$(PK_T)-unpack: .$(PK_T)-unpack

.$(PK_T)-unpack: .$(PK_T)-get-patch
	@mkdir -p $(BLDDIR)
	@touch .$(subst .,,$@)

# Apply patches
$(PK_T)-patch: .$(PK_T)-patch

.$(PK_T)-patch: .$(PK_T)-unpack
	@touch .$(subst .,,$@)

$(PK_T)-init: .$(PK_T)-init 

.$(PK_T)-init:
	@make build-verify
	@touch .$(subst .,,$@)

$(PK_T)-config: .$(PK_T)-config

.$(PK_T)-config:
	@touch .$(subst .,,$@)

$(PK_T): .$(PK_T)

.$(PK_T): .$(X264_T) .$(PK_T)-init 
	@$(MSG) "================================================================"
	@$(MSG2) "Building ${PK}" $(EMSG)
	@$(MSG) "================================================================"

# Build the package
	@touch .$(subst .,,$@)

$(PK_T)-files:
	@$(MSG) "================================================================"
	@$(MSG2) "PK Files" $(EMSG)
	@$(MSG) "================================================================"
	@ls -l $(PK_SRCDIR)

# Package it as an opkg 
pkg $(PK_T)-pkg: .$(PK_T) 
	@make --no-print-directory root-verify opkg-verify
	@mkdir -p $(PKGDIR)/opkg/$(PK)/CONTROL
	@mkdir -p $(PKGDIR)/opkg/$(PK)/etc
	@cp $(SRCDIR)/configs/pointercal* $(PKGDIR)/opkg/$(PK)/etc/
	@cp $(SRCDIR)/opkg/control $(PKGDIR)/opkg/$(PK)/CONTROL/control
	@cp $(SRCDIR)/opkg/postinst $(PKGDIR)/opkg/$(PK)/CONTROL/postinst
	@cp $(SRCDIR)/opkg/debian-binary $(PKGDIR)/opkg/$(PK)/CONTROL/debian-binary
	@chmod +x $(PKGDIR)/opkg/$(PK)/CONTROL/postinst
	@chown -R root:root $(PKGDIR)/opkg/$(PK)/
	@cd $(PKGDIR)/opkg/ && $(OPKG_DIR)/opkg-build -O $(PK)
	@cp $(PKGDIR)/opkg/*.opk $(PKGDIR)/
	@rm -rf $(PKGDIR)/opkg

# Clean the packaging
pkg-clean $(PK_T)-pkg-clean:
	@if [ "$(PKGDIR)" != "" ] && [ -d "$(PKGDIR)" ]; then rm -rf $(PKGDIR); fi

# Clean out a cross compiler build but not the CT-NG package build.
$(PK_T)-clean: $(PK_T)-pkg-clean
	@if [ "$(PK_SRCDIR)" != "" ] && [ -d "$(PK_SRCDIR)" ]; then \
		cd $(PK_SRCDIR) && make distclean; \
	fi
	@rm -f .$(PK_T) 

# Clean out everything associated with PK
$(PK_T)-clobber: root-verify
	@rm -rf $(PKGDIR) 
	@rm -rf $(BLDDIR)/tmp 
	@rm -rf $(PK_SRCDIR) 
	@rm -f .$(PK_T)-config .$(PK_T)-init .$(PK_T)-patch \
		.$(PK_T)-unpack .$(PK_T)-get .$(PK_T)-get-patch
	@rm -f .$(PK_T) 

