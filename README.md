## Synopsis

Ironmancfg is a configuration app for the Ironman project.

It's intent is to provide post installation configuration to a PiBox Development system in order to operate specifically for use with the Ironman home automation project.

## Build

### Cross compile and packaging

To build the package run the following command.

    sudo make pkg

## Installation

Ironmancfg is packaged in an opkg format.  After building look in the pkg directory for an .opk file.  This is the file to be installed on the target system.

To install the package on the target, copy the file to the system or the SD card that is used to boo the system.  The use the following command.

    opkg install <path to file>/xeoncfg_1.0_arm.opk

If the package has been installed previously, use the following command to reinstall it.

    opkg --force-reinstall install <path to file>/xeoncfg_1.0_arm.opk

## Contributors

To get involved with PiBox, contact the project administrator:
Michael J. Hammel <mjhammel@graphics-muse.org>

## License

0BSD

